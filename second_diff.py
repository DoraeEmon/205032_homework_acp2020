# Date:		2020-04-10
# Student ID:	20205032
# Student Name:	JunGeu.Lee


# import library
import numpy as np
import matplotlib.pyplot as plt

## define function ##

#function to be differentiated
def f(x):
    fx_value=np.sin(x)
    return fx_value
#forward differnce algorithm
def fd(f, x, h):
    fd_value = (f(x+h)-f(x))/h
    return fd_value
def fdd(f,x,h):
    return (fd(f,x+h,h)-fd(f,x,h))/h
#central difference algorithm
def cd(f,x,h):
    cd_value = (f(x+h/2)-f(x-h/2))/h
    return cd_value
def cdd(f,x,h):
    return (cd(f,x+h/2,h)-cd(f,x-h/2,h))/h
#extrapolated difference algorithm
def ed(f,x,h):
    ed_value = (4*cd(f,x,h/2)-cd(f,x,h))/3
    return ed_value
def edd(f,x,h):
    return (4*cdd(f,x,h/2)-cdd(f,x,h))/3

## define variable ##
x = np.linspace(-2*np.pi,2*np.pi,1000)
x_point = np.pi/2+1
h = [1, 0.1, 0.01, 0.001, 0.0001]
y = f(x)


## ploting ##
# conside figuresize
plt.figure(figsize=(12,6))   
## subploting. finaly we get 5 sub figure
### loop start ###

for i in range(1,6):
# define ploting position
    plt.subplot(1, 5, i)

# cohes scale 
       
    plt.xlim(-2*np.pi,2*np.pi)
    plt.ylim(-1,1) 
# title name
    plt.title('h =' + str(h[i-1]))

# plot f(x)
    plt.plot(x,y,'k','x',linewidth = 0.2)

# plot x-y axis
    plt.plot(x,x*0,'k', linewidth = 0.2 )
    plt.plot(x*0,x/np.pi*2,'k', linewidth = 0.2)

# calculate and plot first derivatives where h[i-1]
    y_fdd = fdd(f,x,h[i-1])
    y_cdd = cdd(f,x,h[i-1])
    y_edd = edd(f,x,h[i-1])
    plt.plot(x,y_fdd,'r', linewidth = 4,label='fdd')
    plt.plot(x,y_cdd,'k:', linewidth = 4,label='cdd')
    plt.plot(x,y_edd,'b', linewidth = 3,label='edd',alpha=0.5)
    plt.annotate('fdd='+str(fdd(f,x_point,h[i-1])),(x_point,fdd(f,x_point,h[i-1])),(x_point,0.1) ,arrowprops={'color':'red'}) 
    plt.annotate('cdd='+str(cdd(f,x_point,h[i-1])),(x_point,cdd(f,x_point,h[i-1])),(x_point,-0.2) ,arrowprops={'color':'black'}) 
    plt.annotate('edd='+str(edd(f,x_point,h[i-1])),(x_point,edd(f,x_point,h[i-1])),(x_point,-0.7) ,arrowprops={'color':'blue'}) 
### loop end ###    

plt.legend()

plt.savefig('second_derivatives-JungeuLee')
print('Chek file second_derivatives-JungeuLee') 

